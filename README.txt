This module adds the Dropcoin widget to any content in your page.

SETUP

* Install and enable the module as usual.
* Visit administration page to set your settings (admin/config/services/dropcoin)
    · Token (You must get a private token from https://www.dropcoin.es/ by creating an account)
    · Choose size of the widget
    · Choose entity types and bundles where it should be available

* Optionally visit the entity type field display page to choose where to place the widget