<?php

function dropcoin_admin_settings($form, &$form_state) {
  $default_settings = array(
    'token' => '',
    'size' => 'medium',
  );
  $widget_settings = array_merge($default_settings, variable_get('dropcoin_widget_settings', array()));
  $form['dropcoin_widget_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Dropcoin widget settings'),
    '#tree' => TRUE,
  );
  $form['dropcoin_widget_settings']['token'] = array(
    '#title' => t('Token'),
    '#type' => 'textfield',
    '#description' => t('You need to register in <a href="@url" target="_blank">Dropcoin</a> in order to get your private token.', array('@url' => url('https://www.dropcoin.es/'))),
    '#default_value' => $widget_settings['token'],
    '#required' => TRUE,
  );
  $form['dropcoin_widget_settings']['size'] = array(
    '#title' => t('Size'),
    '#type' => 'radios',
    '#options' => array(
      'micro' => t('Micro'),
      'small' => t('Small'),
      'medium' => t('Medium'),
      'large' => t('Large'),
    ),
    '#default_value' => $widget_settings['size'],
  );
  
  // Show by default most common entity types
  $default_entity_types = array(
    'node',
    'taxonomy_term',
    'user',
  );
  $selected_entity_types = array_merge($default_entity_types, variable_get('dropcoin_selected_entity_types', array()));
  if (isset($form_state['values']['dropcoin_additional_entity_types'])) {
    // Update list if it has been updated in an ajax request
    $selected_entity_types[] = $form_state['values']['dropcoin_additional_entity_types'];
  }
  $form['tab_entities_display'] = array(
    '#type' => 'vertical_tabs',
    '#prefix' => '<div id="entities-display">',
  );
  // Obtain info for all entity types
  $entities_info = entity_get_info();
  $fieldset_weight = 0;
  $additional_entity_types = array('_none_' => t('- Choose -'));
  $save_entity_types = array();
  foreach ($entities_info as $entity_type_key => $entity_type) {
    if (in_array($entity_type_key, $selected_entity_types)) {
      // Add selected entity types to vertical tab as fieldset
      $fieldset_key = "dropcoin_widget_display_{$entity_type_key}";
      $form[$fieldset_key] = array(
        '#type' => 'fieldset',
        '#title' => $entity_type['label'],
        '#collapsible' => TRUE,
        '#collapsed' => FALSE,
        '#tree' => TRUE,
        '#weight' => $fieldset_weight,
        '#group' => 'tab_entities_display',
      );
      $fieldset_weight++;
      $bundle_options = array();
      foreach ($entity_type['bundles'] as $bundle_key => $bundle) {
        // Populate array with bundles
        $bundle_options[$bundle_key] = $bundle['label'];
      }
      // No bundles selected by default
      $default_bundles = array(
        'bundles' => array(),
      );
      $selected_bundles = array_merge($default_bundles, variable_get("dropcoin_widget_display_{$entity_type_key}", array()));
      // Create bundle checkboxes
      $form[$fieldset_key]['bundles'] = array(
        '#type' => 'checkboxes',
        '#options' => $bundle_options,
        '#title' => t('Choose the bundles where to display the widget'),
        '#default_value' => $selected_bundles['bundles'],
      );
      $save_entity_types[] = $entity_type_key;
    }
    else {
      // Populate array with entity types that are not used at the moment
      $additional_entity_types[$entity_type_key] = $entity_type['label'];
    }
  }
  variable_set('dropcoin_selected_entity_types', $save_entity_types);
  // Create select list to allow user add further entity types to the field set and choose from its bundles
  $form['dropcoin_additional_entity_types'] = array(
    '#type' => 'select',
    '#title' => t('Add another entity type'),
    '#options' => $additional_entity_types,
    '#description' => t('Optional. Choose additional entity types where to show the widget.  Entity types with no bundles selected will be removed from the list after submitting the form.'),
    '#default_value' => '_none_',
    '#ajax' => array(
      'callback' => 'dropcoin_ajax_add_entity_type',
      'wrapper' => 'entities-display',
    ),
    '#suffix' => '</div>', // Close div#entities-display.
  );
  
  $system_form =  system_settings_form($form);
  $system_form['#submit'][] = 'dropcoin_admin_settings_submit';
  return $system_form;
}

/* Custom submit handler.
 * Check for entity types left empty (no bundles selected) and remove them from the settings
 */
function dropcoin_admin_settings_submit($form, &$form_state) {
  // Retrieve saved entity types
  $selected_entity_types = variable_get('dropcoin_selected_entity_types', array());
  $save_entity_types = array();
  foreach ($selected_entity_types as $entity_type_key) {
    if (isset($form_state['values']["dropcoin_widget_display_{$entity_type_key}"])) {
      // Entity type has been submitted and contains bundles
      foreach ($form_state['values']["dropcoin_widget_display_{$entity_type_key}"]['bundles'] as $bundle_key => $bundle_value) {
        if ($bundle_value !== 0) {
          // Selected entity bundle found, no need to continue iterating
          $save_entity_types[] = $entity_type_key;
          break;
        }
      }
    }
  }
  $empty_entity_types = array_diff($selected_entity_types, $save_entity_types);
  foreach ($empty_entity_types as $entity_type_key) {
    // Delete empty variables from the system
    variable_del("dropcoin_widget_display_{$entity_type_key}");
  }
  // Update list of selected entity types
  variable_set('dropcoin_selected_entity_types', $save_entity_types);
}

function dropcoin_ajax_add_entity_type($form, $form_state) {
  // Rebuild vertical tab and select list
  return array($form['tab_entities_display'], $form['dropcoin_additional_entity_types']);
}
